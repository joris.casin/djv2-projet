using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class StartFight
{
    public static Pokemon active;
    public static Pokemon enemy;
    public static Trainer playerTrainer;
    public static GameObject eventSystem;
    public static Pokemon activeCopy;
    public static Pokemon enemyCopy;
    public static bool isFight = false;
}
