using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Fight : MonoBehaviour
{
    private Pokemon active;
    private Pokemon enemy;
    private Trainer playerTrainer;
    [SerializeField] private Transform enemyPos;
    [SerializeField] private Transform activePos;
    
    void Start()
    {
        active = Instantiate(StartFight.active, activePos.transform);
        active.GetComponent<NavMeshAgent>().enabled = false;
        playerTrainer = StartFight.playerTrainer;
        enemy= Instantiate(StartFight.enemy, enemyPos.transform);
        enemy.GetComponent<NavMeshAgent>().enabled = false;
        active.gameObject.SetActive(true);
        enemy.gameObject.SetActive(true);
        active.transform.localPosition = new Vector3(0, 0, 0);
        enemy.transform.localPosition = new Vector3(0, 0, 0);
        StartFight.activeCopy = active;
        StartFight.enemyCopy = enemy;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
