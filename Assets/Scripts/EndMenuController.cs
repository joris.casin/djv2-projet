using UnityEngine;

public class EndMenuController : MonoBehaviour {
    [SerializeField] private GameObject endMenu;

    public void Start() {
        ShowEndMenu();
    }

    public void QuitToDesktopButton() {
        // Quit Game
        Application.Quit();
    }

    private void ShowEndMenu() {
        // Show End Menu
        endMenu.SetActive(true);
    }

    public void PlayAgainButton() {
        // Start Main Scene again
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}