using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

public class Spawner : MonoBehaviour
{
    public int nbPoke = 0;
    [SerializeField] private int nbPokemonMax = 2;
    [SerializeField] private float cowldown = 5f;
    private float TimeLeft = 0f;
    [SerializeField] private Pokemon pokemon;


    // Update is called once per frame
    void Update()
    {
        TimeLeft -= Time.deltaTime;
        if (!StartFight.isFight&&TimeLeft <= 0f && nbPoke <= nbPokemonMax)
        {
            nbPoke++;
            TimeLeft = cowldown;
            float xRand = Random.Range(-5,5);
            float zRand = Random.Range(-9, 9);
            Pokemon poke=Instantiate(pokemon, transform.position + Vector3.forward * zRand + Vector3.right * xRand, Quaternion.identity);
            poke.spawner = this;
            poke.isWild = true;
        }
    }
}
