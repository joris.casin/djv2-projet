using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements.Experimental;

public class MenuUsePokemon : MonoBehaviour
{
    public Pokemon pokemon;
    private Trainer playerTrainer;
    
    private void Start()
    {
        playerTrainer = GameObject.FindWithTag("Player").GetComponent<Trainer>();
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameObject.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            playerTrainer.pokemons.Remove(pokemon);
            playerTrainer.pokemons.Insert(0, pokemon);
            gameObject.SetActive(false);
            gameObject.transform.parent.gameObject.SetActive(false);
            ToogleUI.Unpause();
        }
    }
}
