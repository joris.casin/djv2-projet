using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Menu : MonoBehaviour
{
    private int state = 0;

    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private GameObject MenuPokemon;
    [SerializeField] private GameObject MenuObjets;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (state%2== 0)
            {
                MenuPokemon.SetActive(true);
            }
            else
            {
                MenuObjets.SetActive(true);
            }

            gameObject.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.W))
        {
            state++;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            state--;
        }

        if (state%2== 0)
        {
            text.text = "> Pokémons\n\n   Objets";
        }
        else
        {
            text.text = "   Pokémons\n\n> Objets";
        }
        
    }
}
