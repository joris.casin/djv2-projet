using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class MenuFight : MonoBehaviour
{
    private Pokemon enemy;

    private Pokemon active;
    private Trainer playerTrainer;
    private int index =0;
    [SerializeField] private TextMeshProUGUI text;
    public bool disabled = false;

    private List<String> list = new List<string>();
    [SerializeField] private MenuPokeball menuPokeball;
    [SerializeField] private GameObject Log;
    [SerializeField] private UIPokemonLog UIlog;
    [SerializeField] private GameObject NamingUI;
    [SerializeField] private TMP_InputField inputField;

    void Start()
    {
        enemy = StartFight.enemyCopy;
        active = StartFight.activeCopy;
        list.Add("Attaquer");
        list.Add("Distraire");
        list.Add("Se concentrer");
        list.Add("Pokeball");
    }

    // Update is called once per frame
    void Update()
    {
        if (!disabled)
        {
            int count = 4;
            if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.W))
            {
                index = (index -1 + count)% count ;
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
            {
                index = (index +1)% count;
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                disabled = true;
                switch (index)
                {
                    case 0:
                        StartCoroutine(AttackChoice());
                        break;
                    case 1:
                        StartCoroutine(DistractionChoice());
                        break;
                    case 2:
                        StartCoroutine(ConcentrationChoice());
                        break;
                    case 3:
                        menuPokeball.gameObject.SetActive(true);
                        break;
                }
            }
            string str = "";
            int variantStart = 0;
            int variantEnd = 0;
            for (int i = 0; i < 4; i++)
            {
                if (i==index)
                {
                    str+="> "+list[i]+ "\n\n";
                }
                else
                {
                    str+="   "+list[i]+ "\n\n";
                }
            }
            text.text = str;
        }
        
    }

    private int CalcDamage(Pokemon atk, Pokemon def)
    {
        float mult = 0;
        switch (atk.pokeData.type)
        {
            case PokemonType.Eau:
                switch (def.pokeData.type)
                {
                    case PokemonType.Eau :
                        mult = 0.5f;
                        break;
                    case PokemonType.Plante :
                        mult = 0.5f;
                        break;
                    case PokemonType.Feu:
                        mult = 2f;
                        break;
                }
                break;
            case PokemonType.Feu:
                switch (def.pokeData.type)
                {
                    case PokemonType.Eau :
                        mult = 0.5f;
                        break;
                    case PokemonType.Plante :
                        mult = 2f;
                        break;
                    case PokemonType.Feu:
                        mult = 0.5f;
                        break;
                }
                break;
            case PokemonType.Plante:
                switch (def.pokeData.type)
                {
                    case PokemonType.Eau :
                        mult = 2f;
                        break;
                    case PokemonType.Plante :
                        mult = 0.5f;
                        break;
                    case PokemonType.Feu:
                        mult = 0.5f;
                        break;
                }
                break;
        }

        return Mathf.CeilToInt((float)atk.pokeData.atk*atk.atkMult / (def.pokeData.def*def.defMult) *40*mult);
    }

    public void TryCatch(bool upgrade)
    {
        this.upgrade = upgrade;
        StartCoroutine(CatchChoice());
    }

    private bool secondAction = false;
    IEnumerator AttackChoice()
    {
        Log.SetActive(true);
        yield return null;
        if (enemy.pokeData.spe<active.pokeData.spe || ((enemy.pokeData.spe == active.pokeData.spe) && (UnityEngine.Random.Range(0f,1f) < 0.5f)))
        { 
            StartCoroutine(AllyAttack());
            while (!secondAction)
            {
                yield return null;
            }
            if ((enemy.currentHP <= 0))
            {
                secondAction = false;
                StartCoroutine(Loot());
            }
            else
            {
                StartCoroutine(EnemyAttack());
                while (secondAction)
                {
                    yield return null;
                }
            }

        }
        else
        {
            StartCoroutine(EnemyAttack());
            while (!secondAction)
            {
                yield return null;
            }
            StartCoroutine(AllyAttack());
            while (secondAction)
            {
                yield return null;
            }
            if ((enemy.currentHP <= 0))
            {
                StartCoroutine(Loot());
            }
        }
        disabled = (enemy.currentHP<=0);
        Log.SetActive(enemy.currentHP <= 0);
        yield return null;
    }

    IEnumerator DistractionChoice()
    {
        Log.SetActive(true);
        if (enemy.pokeData.spe < active.pokeData.spe || ((enemy.pokeData.spe == active.pokeData.spe) && (UnityEngine.Random.Range(0f, 1f) < 0.5f)))
        {
            StartCoroutine(Distraction());
            while (!secondAction)
            {
                yield return null;
            }
            StartCoroutine(EnemyAttack());
            while (secondAction)
            {
                yield return null;
            }
        }
        else
        {
            StartCoroutine(EnemyAttack());
            while (!secondAction)
            {
                yield return null;
            }
            StartCoroutine(Distraction());
            while (secondAction)
            {
                yield return null;
            }
        }
        disabled = false;
        Log.SetActive(false);
        yield return null;
    }

    IEnumerator ConcentrationChoice()
    {
        Log.SetActive(true);
        if (enemy.pokeData.spe < active.pokeData.spe || ((enemy.pokeData.spe == active.pokeData.spe) && (UnityEngine.Random.Range(0f, 1f) < 0.5f)))
        {
            StartCoroutine(Concentration());
            while (!secondAction)
            {
                yield return null;
            }
            StartCoroutine(EnemyAttack());
            while (secondAction)
            {
                yield return null;
            }
        }
        else
        {
            StartCoroutine(EnemyAttack());
            while (!secondAction)
            {
                yield return null;
            }
            StartCoroutine(Concentration());
            while (secondAction)
            {
                yield return null;
            }
        }
        disabled = false;
        Log.SetActive(false);
        yield return null;
    }

    private bool upgrade;

    IEnumerator CatchChoice()
    {
        Log.SetActive(true);
        StartCoroutine(Catch());
        while (!secondAction)
        {
            yield return null;
        }
        StartCoroutine(EnemyAttack());
        while (secondAction)
        {
            yield return null;
        }
        disabled = false;
        Log.SetActive(false);
        yield return null;
    }

    public bool endMessage = false;
    IEnumerator AllyAttack()
    {
        UIlog.messagetarget = active.name + " attaque.";
        enemy.TakeDamage(CalcDamage(active, enemy));
        while (!endMessage)
        {
            yield return null;
        }
        endMessage = false;
        secondAction = !secondAction;
        yield return null;
    }

    IEnumerator EnemyAttack()
    {
        UIlog.messagetarget = enemy.name + " attaque.";
        active.TakeDamage(CalcDamage(enemy, active));
        while (!endMessage)
        {
            yield return null;
        }
        endMessage = false;
        secondAction = !secondAction;
        yield return null;
    }

    IEnumerator Distraction()
    {
        UIlog.messagetarget = active.name + " distrait " + enemy.name + ".";
        while (!endMessage)
        {
            yield return null;
        }
        endMessage = false;
        enemy.Modifier(2f / 3f);
        UIlog.messagetarget = "L'attaque et la defense de " + enemy.name + " chute!";
        while (!endMessage)
        {
            yield return null;
        }
        endMessage = false;
        secondAction = !secondAction;
        yield return null;
    }

    IEnumerator Concentration()
    {
        UIlog.messagetarget = active.name + " se concentre.";
        while (!endMessage)
        {
            yield return null;
        }
        endMessage = false;
        enemy.Modifier(1.5f);
        UIlog.messagetarget = "L'attaque et la defense de " + active.name + " augmente!";
        while (!endMessage)
        {
            yield return null;
        }
        endMessage = false;
        secondAction = !secondAction;
        yield return null;
    }

    public bool end = false;
    IEnumerator Catch()
    {
        UIlog.messagetarget = "Vous essayez de capturer "+enemy.name+".";
        while (!endMessage)
        {
            yield return null;
        }
        endMessage = false;
        bool success;
        if (upgrade)
        {
            success = (UnityEngine.Random.Range(0f, 1f) <= (2f / 3f +(1f- 1f / 3f * enemy.currentHP / enemy.pokeData.hp)));
            StartFight.playerTrainer.RemoveItem("Pokeball +");
        }
        else
        {
            success = (UnityEngine.Random.Range(0f, 1f) <= (1f / 3f +(1f- 2f / 3f * enemy.currentHP / enemy.pokeData.hp)));
            StartFight.playerTrainer.RemoveItem("Pokeball");
        }
        if (success)
        {
            UIlog.messagetarget = "Vous avez capture le Pokémon.";
            while (!endMessage)
            {
                yield return null;
            }
            endMessage = false;
            NamingUI.SetActive(true);
            NamingUI.GetComponent<NamingUI>().hp= enemy.currentHP;
            inputField.text=enemy.name;
            while (!end)
            {
                yield return null;
            }
            StartFight.active.currentHP = StartFight.activeCopy.currentHP;
            StartFight.active.atkCD = Math.Max(StartFight.activeCopy.atkCD - 1, 0);
            StartFight.active.defCD = Math.Max(StartFight.activeCopy.defCD - 1, 0);
            StartFight.eventSystem.SetActive(true);
            StartFight.isFight = false;
            StartFight.enemy.Hide(true);
            SceneManager.UnloadSceneAsync("FightingScene", UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        }
        else
        {
            UIlog.messagetarget = "Vous n'avez pas reussi a capturer le Pokémon.";
            while (!endMessage)
            {
                yield return null;
            }
            endMessage = false;
            secondAction = !secondAction;
        }
        yield return null;
    }

    IEnumerator Loot()
    {
        UIlog.messagetarget = "Vous avez battu le Pokémon et obtenu "+enemy.loot+".";
        while (!endMessage)
        {
            yield return null;
        }
        StartFight.active.currentHP = StartFight.activeCopy.currentHP;
        StartFight.active.atkCD = Math.Max(StartFight.activeCopy.atkCD - 1, 0);
        StartFight.active.defCD = Math.Max(StartFight.activeCopy.defCD - 1, 0);
        StartFight.eventSystem.SetActive(true);
        StartFight.isFight = false;
        SceneManager.UnloadSceneAsync("FightingScene", UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
        yield return null;
    }
}
