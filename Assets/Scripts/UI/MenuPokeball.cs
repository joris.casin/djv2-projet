using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using TMPro;

public class MenuPokeball : MonoBehaviour
{
    
    private Trainer playerTrainer;
    private int index =0;
    [SerializeField] private TextMeshProUGUI text;
    int count;
    private List<String> itemList = new List<string>();

    private MenuFight menuFight;
    // Start is called before the first frame update
    void Start()
    {
        playerTrainer = GameObject.FindWithTag("Player").GetComponent<Trainer>();
        itemList.Add("Pokeball");
        itemList.Add("Pokeball +");
        count = itemList.Count;
        menuFight = GetComponentInParent<MenuFight>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.W))
        {
            index = (index -1 + count)% count ;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            index = (index +1)% count;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameObject.SetActive(false);
            GetComponentInParent<MenuFight>().disabled = false;
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (playerTrainer.items.TryGetValue(itemList[index], out var nb))
            {
                if (nb > 0)
                {
                    switch (itemList[index])
                    {
                        case "Pokeball" :
                            menuFight.TryCatch(false);
                            break;
                        case "Pokeball +" :
                            menuFight.TryCatch(true);
                            break;
                    }
                    gameObject.SetActive(false);
                }
            }
        }
        
        string str = "";
        int i = 0;
        foreach (var item in playerTrainer.items)
        {
            if (item.Key == "Pokeball" || item.Key == "Pokeball +")
            {
                if (i==index)
                {
                    str +="> "+ item.Key + " x" + item.Value+ "\n\n";

                }
                else if (i >= 0 && i <2)
                {
                    str+="   " + item.Key + " x" + item.Value+ "\n\n";
                }

                i++;
                
            }
        }
        text.text = str;
        
    }
}
