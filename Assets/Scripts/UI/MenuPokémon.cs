using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MenuPokémon : MonoBehaviour
{
    private Trainer playerTrainer;
    private int index =0;
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private MenuUsePokemon menuUsePokemon;

    private void Start()
    {
        playerTrainer = GameObject.FindWithTag("Player").GetComponent<Trainer>();
    }

    // Update is called once per frame
    void Update()
    {
        int count = playerTrainer.pokemons.Count;
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.W))
        {
            index = (index -1 + count)% count ;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            index = (index +1)% count;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameObject.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            menuUsePokemon.gameObject.SetActive(true);
            menuUsePokemon.pokemon = playerTrainer.pokemons[index];
        }

        string str = "";
        int variantStart = 0;
        int variantEnd = 0;
        if (index == 0)
        {
            variantEnd = 1;
        }
        if (index == count-1)
        {
            variantStart = 1;
        }
        for (int i = Math.Max(0, index-1-variantStart); i < Math.Min(count, index+2+variantEnd); i++)
        {
            if (i==index)
            {
                str+="> "+ playerTrainer.pokemons[i].name + "\n\n";
            }
            else
            {
                str+="   "+ playerTrainer.pokemons[i].name + "\n\n";
            }
        }
        

        text.text = str;
    }
}
