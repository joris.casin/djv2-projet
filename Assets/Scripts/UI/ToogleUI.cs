using System.Collections;
using System.Collections.Generic;
using UnityChan;
using UnityEditor;
using UnityEngine;

public class ToogleUI : MonoBehaviour
{
    [SerializeField] private GameObject menuUI;
    [SerializeField] private GameObject player;
    public static bool isPause = false;
    private static UnityChanControlScriptWithRgidBody controller;
    // Start is called before the first frame update
    void Start()
    {
            player = GameObject.FindWithTag("Player");
            controller = player.GetComponent<UnityChanControlScriptWithRgidBody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !StartFight.isFight)
        {
            menuUI.SetActive(!menuUI.activeSelf);
            isPause = menuUI.activeSelf;
        }
        controller.enabled = !isPause;
    }

    public static void Pause()
    {
        isPause = true;
    }
    public static void Unpause()
    {
        isPause = false;
    }
}
