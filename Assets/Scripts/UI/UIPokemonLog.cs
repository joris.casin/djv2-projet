using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using TMPro;
using UnityEngine;

public class UIPokemonLog : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    public string messagetarget;
    private int index=0;
    [SerializeField] private float cd = 0.01f;
    private float TimeLeft = 0f;

    [SerializeField] private MenuFight menufight;
    void Update()
    {
        if (index<messagetarget.Length)
        {
            TimeLeft -= Time.deltaTime;
            if (TimeLeft < 0f) {
                TimeLeft = cd;
                text.text += messagetarget[index];
                index++;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            { 
                clear();
                menufight.endMessage = true;
            }
        }
        
    }
    public void clear()
    {
        index = 0;
        text.text = "";
    }
}
