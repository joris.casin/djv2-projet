using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NamingUI : MonoBehaviour
{
    [SerializeField] private TMP_InputField inputField;
    public int hp;
    [SerializeField] private MenuFight menufight;
    void Update()
    {
        if ((Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter)) && inputField.text.Length>0) 
        {
            StartFight.enemy.currentHP = hp;
            StartFight.enemy.name = inputField.text;
            StartFight.playerTrainer.AddPokemon(StartFight.enemy);
            menufight.end = true;
        }
    }
}
