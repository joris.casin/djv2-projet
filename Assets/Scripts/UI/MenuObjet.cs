using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TMPro;
using UnityEngine;

public class MenuObjet : MonoBehaviour
{
    private Trainer playerTrainer;
    private int index =0;
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private MenuUseObjet menuUseObjet;
    private List<String> itemList = new List<string>();
    int count;

    private void Start()
    {
        playerTrainer = GameObject.FindWithTag("Player").GetComponent<Trainer>();
        itemList.Add("Potion");
        itemList.Add("Potion +");
        itemList.Add("Pokeball");
        itemList.Add("Pokeball +");
        itemList.Add("+Attaque");
        itemList.Add("+Defense");
        count = playerTrainer.items.Count;
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var item in playerTrainer.items)
        {
            switch (item.Key)
            {
                case "Potion":
                    break;
                case "Potion +" :
                    break;
                
                
            }
        }
        
        if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.W))
        {
            index = (index -1 + count)% count ;
        }
        else if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
        {
            index = (index +1)% count;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameObject.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (playerTrainer.items.TryGetValue(itemList[index], out var nb))
            {
                if (nb > 0)
                {
                    switch (itemList[index])
                    {
                        case "Pokeball" :
                            break;
                        case "Pokeball +" :
                            break;
                        default:
                            menuUseObjet.gameObject.SetActive(true);
                            menuUseObjet.item = itemList[index];
                            break;
                    }
                }
            }
        }
        
        string str = "";
        int variantStart = 0;
        int variantEnd = 0;
        if (index == 0)
        {
            variantEnd = 1;
        }
        if (index == count-1)
        {
            variantStart = 1;
        }

        int i = 0;
        foreach (var item in playerTrainer.items)
        {
            if (i==index)
            {
                str +="> "+ item.Key + " x" + item.Value+ "\n\n";

            }
            else if (i >= Math.Max(0, index-1-variantStart) && i <Math.Min(count, index+2+variantEnd))
            {
                str+="   " + item.Key + " x" + item.Value+ "\n\n";
            }

            i++;
        }
        

        text.text = str;
        text.fontSize = 12;
    }
    
}
