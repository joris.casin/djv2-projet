using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuUseObjet : MonoBehaviour
{
    public string item;
    private Trainer playerTrainer;
    [SerializeField] private MenuSelectPokemon menuSelectPokemon;

    private void Start()
    {
        playerTrainer = GameObject.FindWithTag("Player").GetComponent<Trainer>();
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            gameObject.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            menuSelectPokemon.gameObject.SetActive(true);
            menuSelectPokemon.item = item;
        }
    }
}
