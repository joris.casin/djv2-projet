using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class Trainer : MonoBehaviour
{
    public List<Pokemon> pokemons = new List<Pokemon>();
    public Dictionary<string, int> items = new Dictionary<string, int>();
    // Start is called before the first frame update
    void Start()
    {
        items.Add("Potion",0);
        items.Add("Potion +",0);
        items.Add("Pokeball",0);
        items.Add("Pokeball +",0);
        items.Add("+Attaque",0);
        items.Add("+Defense",0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AddPokemon(Pokemon p)
    {
        pokemons.Add(p);
    }
    
    public void AddItem(string i)
    {
        int nb = 0;
        items.TryGetValue(i, out nb);
        items[i] =nb+1;
    }
    public void RemoveItem(string i)
    {
        int nb = 0;
        items.TryGetValue(i, out nb);
        if (nb >0) items[i] = nb-1;
    }

    public void UseItem(string item, Pokemon pokemon)
    {
        switch (item)
        {
            case "Potion":
                pokemon.Heal(50);
                break;
            case "Potion +":
                pokemon.Heal(100);
                break;
            case "+Attaque":
                pokemon.Buff(true);
                break;
            case "+Defense":
                pokemon.Buff(false);
                break;
        }
    }
}
