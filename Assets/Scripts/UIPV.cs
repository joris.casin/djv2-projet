using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIPV : MonoBehaviour
{
    public Pokemon pokemon;
    [SerializeField] private RectTransform rect;

    [SerializeField]
    private TextMeshProUGUI Name;

    [SerializeField] private bool isAllied;

    private void Start()
    {
        if (isAllied)
        {
            pokemon = StartFight.activeCopy;
        }
        else
        {
            pokemon = StartFight.enemyCopy;
        }
    }

    void Update()
    {
        float target = ((float)(pokemon.currentHP))/(float)(pokemon.pokeData.hp);
        Name.text = pokemon.name;
        if (Time.deltaTime < (Mathf.Abs(rect.anchorMax.x-target)))
        {
            if(rect.anchorMax.x > target)
            {
                rect.anchorMax -= new Vector2(Time.deltaTime,0f);
            }
            else
            {
                rect.anchorMax += new Vector2(Time.deltaTime, 0f);
            }
        }
        else
        {
            rect.anchorMax = new Vector2(target, 0.5f);
        }
    }
}
