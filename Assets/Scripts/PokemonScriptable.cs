using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum PokemonType
{
    Eau,
    Feu,
    Plante
}

[CreateAssetMenu]
public class PokemonScriptable : ScriptableObject
{
    public int hp = 100;
    public int atk = 1;
    public int def = 1;
    public int spe = 1;
    public PokemonType type = PokemonType.Eau;
    public GameObject prefab;
    public string commonLoot;
    public string rareLoot;





}
