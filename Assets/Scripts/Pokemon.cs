using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class Pokemon : MonoBehaviour
{
    public PokemonScriptable pokeData;
    public int atkCD = 0;
    public int defCD = 0;
    public Spawner spawner;
    private Collider playerCollider;
    public bool isWild = false;
    private GameObject eventSystem;
    public int currentHP = 100;
    public float atkMult=1;
    public float defMult=1;
    public String name = "";
    public String loot = "";
    private NavMeshAgent agent;
    private float cdMove = 10f;
    private float TimeLeft = 0f;
    private Transform PokemonList;
    public void Start()
    {
        PokemonList = GameObject.FindWithTag("PokemonList").transform;
        agent = GetComponent<NavMeshAgent>();
        if (isWild)
        {
            GameObject playerObject = GameObject.FindWithTag("Player");
            if (playerObject)
            {
                playerCollider = playerObject.GetComponent<Collider>();
            }

            if (StartFight.eventSystem)
            {
                eventSystem = StartFight.eventSystem;
            }
            else
            {
                eventSystem = GameObject.FindWithTag("EventSystem");
            }

        }
    }

    public void Update()
    {
        TimeLeft-=Time.deltaTime;
        if (TimeLeft < 0f && !StartFight.isFight)
        {
            float xRand = Random.Range(-5, 5);
            float zRand = Random.Range(-9, 9);
            TimeLeft = cdMove + UnityEngine.Random.Range(-2f, 2f);
            agent.SetDestination(spawner.transform.position+ Vector3.forward * zRand + Vector3.right * xRand);
        }
    }
    public void Heal(int n)
    {
        currentHP = Math.Min(currentHP + n, pokeData.hp);
    }

    public void Buff(bool atk)
    {
        if (atk)
        {
            atkMult = 1.5f;
            atkCD = 3;
        }
        else
        {
            defMult = 1.5f;
            defCD = 3;
        }
    }
    
    public void TakeDamage(int n)
    {
        currentHP = Math.Max(currentHP-n, 0);
        if (currentHP == 0)
        {
            if (this == StartFight.enemyCopy)
            {
                if (Random.Range(0f, 1f) > 0.1)
                {
                    StartFight.playerTrainer.AddItem(pokeData.commonLoot);
                    loot = pokeData.commonLoot;
                }
                else
                {
                    StartFight.playerTrainer.AddItem(pokeData.rareLoot);
                    loot = pokeData.rareLoot;
                }
                StartFight.enemy.Hide(false);
            }
            if (StartFight.activeCopy.currentHP <= 0)
            {
                StartFight.playerTrainer.pokemons.Remove(StartFight.active);
                if (StartFight.playerTrainer.pokemons.Count <= 0)
                {
                    SceneManager.LoadScene("End");
                }
                StartFight.active.currentHP = StartFight.activeCopy.currentHP;
                StartFight.active.atkCD = Math.Max(StartFight.activeCopy.atkCD - 1, 0);
                StartFight.active.defCD = Math.Max(StartFight.activeCopy.defCD - 1, 0);
                StartFight.eventSystem.SetActive(true);
                StartFight.isFight = false;
                StartFight.enemy.Hide(false);
                SceneManager.UnloadSceneAsync("FightingScene", UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
            }
           
        }

    }

    public void Modifier(float modif)
    {
        atkMult *= modif;
        defMult *= modif;
    }
    
    public void OnTriggerEnter(Collider other)
    {
        if (isWild && playerCollider == other && !StartFight.isFight)
        {
            StartFight.playerTrainer = playerCollider.gameObject.GetComponent<Trainer>();
            StartFight.active= StartFight.playerTrainer.pokemons[0];
            StartFight.enemy = this;
            isWild = false;
            eventSystem.SetActive(false);
            StartFight.eventSystem = eventSystem;
            SceneManager.LoadScene("FightingScene",LoadSceneMode.Additive);
            StartFight.isFight= true;
        }
    }

    
    private void OnDestroy()
    {
        if (isWild)
        {
            spawner.nbPoke--;
        }
    }

    public void Hide(bool isCatch)
    {
        spawner.nbPoke--;
        gameObject.SetActive(false);
        if(isCatch) transform.SetParent(PokemonList);
    }

}
