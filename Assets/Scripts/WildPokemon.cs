using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WildPokemon : MonoBehaviour
{
    public PokemonScriptable pokeData;
    public Spawner spawner;
    private Collider playerCollider;
    public bool isWild = true;
    
    public void Start()
    {
        GameObject playerObject = GameObject.FindWithTag("Player");
        if (playerObject)
        {
            playerCollider = playerObject.GetComponent<Collider>();
        }
    }
    
    public void OnTriggerEnter(Collider other)
    {
        if (playerCollider == other) 
        {
            SceneManager.LoadScene("FightingScene",LoadSceneMode.Additive);
        }
    }

    private void OnDestroy()
    {
        if (isWild)
        {
            spawner.nbPoke--;
        }
    }
}
